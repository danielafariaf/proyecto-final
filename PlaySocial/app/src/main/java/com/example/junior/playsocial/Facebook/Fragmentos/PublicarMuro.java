package com.example.junior.playsocial.Facebook.Fragmentos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.junior.playsocial.Facebook.Controladores.ControladorFacebook;
import com.example.junior.playsocial.PerfilGeneral;
import com.example.junior.playsocial.R;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;


public class PublicarMuro extends Fragment {



    public PublicarMuro() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_publicar_muro, container, false);
        final EditText texto =(EditText)view.findViewById(R.id.descripcionShare);
        final EditText web = (EditText)view.findViewById(R.id.direccionShare);
        Button boton = (Button)view.findViewById(R.id.botonMuro);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ControladorFacebook.publicarMuro(PublicarMuro.this,texto.getText().toString(),web.getText().toString());
            }
        });




        return view;
    }




}

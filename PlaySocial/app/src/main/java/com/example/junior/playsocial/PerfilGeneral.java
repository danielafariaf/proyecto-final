package com.example.junior.playsocial;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.transition.Visibility;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.example.junior.playsocial.Facebook.Controladores.ControladorFacebook;
import com.example.junior.playsocial.Facebook.Fragmentos.FotosFragmento;
import com.example.junior.playsocial.Facebook.Fragmentos.ListaAmigosFragmento;
import com.example.junior.playsocial.Facebook.Fragmentos.PrincipalFragmentoF;
//<<<<<<< HEAD
import com.example.junior.playsocial.GooglePlus.Actividades.PrincipalGoogle;
//=======
import com.example.junior.playsocial.Facebook.Fragmentos.PublicarMuro;
//>>>>>>> e6bb21f8da3983f47cd7cfc2ba89c847e838ff0d
import com.example.junior.playsocial.GooglePlus.Fragmentos.PrincipalFragmentoG;
import com.example.junior.playsocial.Instagram.Activities.InstagramPrincipalActivity;
import com.example.junior.playsocial.Instagram.Controladores.ControladorInstagram;
import com.example.junior.playsocial.Instagram.Fragmentos.FragmentIterationListener;
import com.example.junior.playsocial.Instagram.Fragmentos.PrincipalFragmentoI;
import com.facebook.login.LoginManager;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

public class PerfilGeneral extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.OnConnectionFailedListener,
        FragmentIterationListener {
    private GoogleApiClient googleApiClient;

    private static final int SELECT_FILE = 1;
    public static final String TAG = "PlaySocial";
    private ControladorInstagram controladorInstagram;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_general);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //-------------------------------------------------------------------------

        this.controladorInstagram = (ControladorInstagram) getIntent().getExtras().getSerializable("controladorInstagram");
        //-----------------------------------------------------------------------------------
        //Colocando el icono en la parte superior izquierda:
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.drawable.logopequeno);
        actionBar.setTitle("");
        //-----------------------------------------------------------------------------------


        //-----------------------------------------------------------------------------------
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //-----------------------------------------------------------------------------------
        //Operaciones Iniciales:
        cargarMenu (navigationView);
        ubicandoHome();


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.perfil_general, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * Aqui se colocan las funciones de cada boton segun la red social en la que se este
     * trabajado.
     * @param item
     * @return
     */
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        //Acciones de cada boton para la red social Facebook
        //-----------------------------------------------------------------------------------------
        if (ControladorGeneral.redactiva==1){

            if (id == R.id.boton0) {
                ubicandoHome();
            }else if (id == R.id.boton1) {
                FragmentManager manager = getFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.content_perfil_general, new ListaAmigosFragmento());
                transaction.commit();
            } else if (id == R.id.boton2) {
                FragmentManager manager = getFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.content_perfil_general, new PublicarMuro());
                transaction.commit();


            } else if (id == R.id.boton3) {


            } else if (id == R.id.botonCerrar) {
                cerrarSesion();
            }

        }

        //Acciones de cada boton para la red social Instagram
        //-----------------------------------------------------------------------------------------
        if (ControladorGeneral.redactiva==2){

            if (id == R.id.boton0) {
                ubicandoHome();
            }else if (id == R.id.boton1) {

            } else if (id == R.id.boton2) {

            } else if (id == R.id.boton3) {

            } else if (id == R.id.botonCerrar) {
                cerrarSesion();
            }

        }

        //Acciones de cada boton para la red social Google Plus
        //-----------------------------------------------------------------------------------------
        if (ControladorGeneral.redactiva==3){

            if (id == R.id.boton0) {
                ubicandoHome();
            }else if (id == R.id.boton1) {

            } else if (id == R.id.boton2) {

            } else if (id == R.id.boton3) {

            } else if (id == R.id.botonCerrar) {

                cerrarSesion();
            }
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Carga el menu de una manera distinta para cada red social
     * @param navigationView
     */
    private void cargarMenu (NavigationView navigationView){


        //Cargando botones para Facebook:
        //------------------------------------------------------------------------------------------
        if (ControladorGeneral.redactiva==1){
            //Colocando icono de arriba
            View cabecera =navigationView.getHeaderView(0);
            ImageView icono = (ImageView)cabecera.findViewById(R.id.iconoRed);
            icono.setBackgroundResource(0);
            icono.setBackgroundResource(R.drawable.facebookicono);
            TextView nombre = (TextView)cabecera.findViewById(R.id.usuarioRed);
            nombre.setText(ControladorFacebook.nombre + " " + ControladorFacebook.apellido);
            Menu menu = navigationView.getMenu();
            //En esta zona se editan los botones del menu (Le colocan los titulos que quieran)
            //-----------------------------------------------------------------------------------
            MenuItem amigos = menu.findItem(R.id.boton1);
            amigos.setTitle("Lista de Amigos");
            amigos.setIcon(R.drawable.usuario);
            MenuItem muro = menu.findItem(R.id.boton2);
            muro.setTitle("Publicar en el Muro");
            muro.setIcon(R.drawable.ic_menu_share);
            MenuItem publicar = menu.findItem(R.id.boton3);
            publicar.setVisible(false);
        }
        //Cargando botones para Instagram:
        //------------------------------------------------------------------------------------------
        if (ControladorGeneral.redactiva==2){
            //Colocando icono de arriba
            View cabecera = navigationView.getHeaderView(0);
            ImageView icono = (ImageView)cabecera.findViewById(R.id.iconoRed);
            icono.setBackgroundResource(0);
            icono.setBackgroundResource(R.drawable.instagramicono);
            Menu menu = navigationView.getMenu();
            //En esta zona se editan los botones del menu (Le colocan los titulos que quieran)
            //-----------------------------------------------------------------------------------
            MenuItem boton1 = menu.findItem(R.id.boton1);
            boton1.setTitle("Opcion 1");
            MenuItem boton2 = menu.findItem(R.id.boton2);
            boton2.setTitle("Opcion 2");
            MenuItem boton3 = menu.findItem(R.id.boton3);
            boton3.setTitle("Opcion 3");

        }
        //Cargando botones para Google Plus:
        //------------------------------------------------------------------------------------------
        if (ControladorGeneral.redactiva==3){
            //Colocando icono de arriba
            View cabecera = navigationView.getHeaderView(0);
            ImageView icono = (ImageView)cabecera.findViewById(R.id.iconoRed);
            icono.setBackgroundResource(0);
            icono.setBackgroundResource(R.drawable.googleicono);
            TextView nombre = (TextView)cabecera.findViewById(R.id.usuarioRed);

            Menu menu = navigationView.getMenu();
            //En esta zona se editan los botones del menu (Le colocan los titulos que quieran)
            //-----------------------------------------------------------------------------------
            MenuItem colecciones = menu.findItem(R.id.boton1);
            colecciones.setTitle("Colecciones");
            MenuItem personas = menu.findItem(R.id.boton2);
            personas.setVisible(false);
            personas.setTitle("");
            MenuItem comunidades = menu.findItem(R.id.boton3);
            comunidades.setVisible(false);
            comunidades.setTitle("");
            MenuItem cerrarS = menu.findItem(R.id.botonCerrar);
            cerrarS.setVisible(false);

        }
    }

    /**
     * Cierra sesion de una manera distinta para cada red social
     */
    private void cerrarSesion(){
        if (ControladorGeneral.redactiva==1){
            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("cookieFacebook","vacio");
            editor.commit();
            LoginManager.getInstance().logOut();

            Intent cerrar = new Intent(PerfilGeneral.this,InicioActivity.class);
            startActivity(cerrar);
            finish();

        }

        if (ControladorGeneral.redactiva==2){
            Intent cerrar = new Intent(PerfilGeneral.this,InstagramPrincipalActivity.class);
            startActivity(cerrar);
            finish();

        }

        if (ControladorGeneral.redactiva==3){

            Intent irgoogle = new Intent(getApplicationContext(), InicioActivity.class);
            irgoogle.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(irgoogle);
              }

    }

    /**
     * Ubica el home del perfil para una red social determinada
     */
    private void ubicandoHome(){
        if (ControladorGeneral.redactiva==1){
            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.content_perfil_general, new PrincipalFragmentoF());
            transaction.commit();

        }

        if (ControladorGeneral.redactiva==2){

            Log.i("CARGAR FRAGMENT", this.controladorInstagram.getUserName());

            PrincipalFragmentoI fragment = PrincipalFragmentoI.newInstance(this.controladorInstagram, getBaseContext());

            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.content_perfil_general, fragment);
            transaction.commit();

        }

        if (ControladorGeneral.redactiva==3){
            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.content_perfil_general, new PrincipalFragmentoG());
            transaction.commit();

        }


    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {


    }

    public void onResume(){
        super.onResume();



    }

}



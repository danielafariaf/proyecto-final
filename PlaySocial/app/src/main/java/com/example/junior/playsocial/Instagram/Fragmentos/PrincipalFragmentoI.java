package com.example.junior.playsocial.Instagram.Fragmentos;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.junior.playsocial.Instagram.Controladores.ControladorInstagram;
import com.example.junior.playsocial.R;
import com.squareup.picasso.Picasso;

import java.net.URL;

import java.lang.ClassCastException;


/**
 * Controlador del Fragmento que muestra los datos del usuario.
 */
public class PrincipalFragmentoI extends android.app.Fragment {
    public static final String TAG = "InstagramProfile";
    private OnFragmentInteractionListener mListener;
    private ControladorInstagram mUSer;
    private ImageView profilePic;
    private TextView userName;
    private TextView userBio;
    private TextView userWeb;
    private TextView userFollows;
    private TextView userFollower;
    private FragmentIterationListener mCallback;
    private static Context mContext;

    public PrincipalFragmentoI() {
        // Required empty public constructor

    }


    // TODO: Rename and change types and number of parameters
    public static PrincipalFragmentoI newInstance(ControladorInstagram controladorInstagram, Context context) {
        Log.i("FRAGMENT",controladorInstagram.getUserName());
        PrincipalFragmentoI fragment = new PrincipalFragmentoI();
        Bundle args = new Bundle();
        args.putSerializable("controladorInstagram", controladorInstagram);
        fragment.setArguments(args);
        mContext = context;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if (getArguments() != null) {
            mUSer = (ControladorInstagram) getArguments().getSerializable("controladorInstagram");
            Log.i("ONCREATE FRAGMENT",mUSer.getUserName());

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fragment_instagram_profile, container, false);

        try {

            profilePic = (ImageView) v.findViewById(R.id.igprofilepic);
            userName = (TextView) v.findViewById(R.id.username);
            userBio = (TextView) v.findViewById(R.id.bio);
            userWeb = (TextView) v.findViewById(R.id.web);
            userFollows = (TextView) v.findViewById(R.id.follows);
            userFollower = (TextView) v.findViewById(R.id.followed_by);

            Log.i("IMAGE URL", mUSer.getProfileImageURL());
            // profilePic.setImageBitmap(LoadImageFromWebOperations(mUSer.getProfileImageURL()));
            Picasso.with(mContext).load(mUSer.getProfileImageURL()).fit().into(profilePic);
            userName.setText(mUSer.getUserName());
            userBio.setText(mUSer.getBio());
            userWeb.setText(mUSer.getWebsite());
            userFollows.setText(String.valueOf(mUSer.getFollowing()));
            userFollower.setText(String.valueOf(mUSer.getFollowers()));

        }
        catch (NullPointerException e){
            Log.i("NULL POINTER ",e.getMessage());
        }

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(ControladorInstagram controladorInstagram) {
        if (mListener != null) {
            mListener.onFragmentInteraction(controladorInstagram);
        }
    }

    //El fragment se ha adjuntado al Activity
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            mCallback = (FragmentIterationListener) activity;
        }catch(ClassCastException ex){
            Log.e("FragInstagramProfile", "El Activity debe implementar la interfaz FragmentIterationListener");
        }
    }

    //El Fragment ha sido quitado de su Activity y ya no está disponible
    @Override
    public void onDetach() {
        super.onDetach();
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(ControladorInstagram controladorInstagram);
    }

    public static Bitmap LoadImageFromWebOperations(String url) {
        URL imageUrl = null;
        try {
            imageUrl = new URL(url);
            Bitmap bmp = BitmapFactory.decodeStream(imageUrl.openConnection().getInputStream());
            return bmp;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}

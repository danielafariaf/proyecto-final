package com.example.junior.playsocial.Facebook.Controladores;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.example.junior.playsocial.Facebook.Fragmentos.ListaAmigosFragmento;
import com.example.junior.playsocial.Facebook.Model.ListaAmigo;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.net.URL;

/**
 * Created by William on 13/7/2017.
 */

public class GetBitmap extends AsyncTask<Void,Void,Void> {

    private Context context;
    private String[] urls;
    private ProgressDialog loading;
    private ListaAmigosFragmento listaAmigosFragmento;

    public GetBitmap(Context context, ListaAmigosFragmento listaAmigosFragmentos, String[] urls){
        this.context = context;
        this.listaAmigosFragmento = listaAmigosFragmentos;
        this.urls = urls;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        loading = ProgressDialog.show(listaAmigosFragmento.getActivity(), "Descargando imagenes","Por favor espere...",
                false,
                false);

    }

    @Override
    protected Void doInBackground(Void... params) {
        for (int i = 0; i < urls.length; i++){
            ListaAmigo.imagenes[i] = getImage(urls[i]);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        loading.dismiss();
        listaAmigosFragmento.showData();
    }

    private Bitmap getImage (String url) {
        try {
            URL fbAvatarUrl = new URL(url);
            HttpGet httpRequest = new HttpGet(fbAvatarUrl.toString());
            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpResponse response = (HttpResponse) httpclient.execute(httpRequest);
            HttpEntity entity = response.getEntity();
            BufferedHttpEntity bufHttpEntity = new BufferedHttpEntity(entity);
            Bitmap fbAvatarBitmap = BitmapFactory.decodeStream(bufHttpEntity.getContent());
            httpRequest.abort();
            return fbAvatarBitmap;
        } catch (Exception e) {

        }
        return null;
    }
}

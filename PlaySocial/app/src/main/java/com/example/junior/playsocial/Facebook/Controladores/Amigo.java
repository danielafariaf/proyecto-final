package com.example.junior.playsocial.Facebook.Controladores;

/**
 * Created by Junior on 12/07/2017.
 */

public class Amigo {
    private String id;
    private String foto;
    private String nombre;


    public Amigo(String id, String foto, String nombre) {
        this.id = id;
        this.foto = foto;
        this.nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}

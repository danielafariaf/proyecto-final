package com.example.junior.playsocial.Facebook.Controladores;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.junior.playsocial.ControladorGeneral;
import com.example.junior.playsocial.InicioActivity;
import com.example.junior.playsocial.PerfilGeneral;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.share.ShareApi;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Junior on 06/06/2017.
 */

public class ControladorFacebook {

    public static String idUsuario;
    public static String tokenAutorizacion;
    public static String appid;
    public static String correo;
    public static String nombre;
    public static String apellido;
    public static String genero;
    public static String cumpleanos;
    public static String id;
    public static String foto;
    public static Bitmap grafico;
    public static String conectado;
    public static String listaAmigos;
    public static Bitmap fotoalbum;
    public static boolean iniciado;



    //------------------PROCESOS EXTRA DE LAS REDES SOCIALES------------------------------------------------
    /**
     * Cargo los datos de la cuenta y los guardo en memoria.
     * @param accessToken
     */
    public static void almacenandoFacebook(final Context context, AccessToken accessToken){
        GraphRequest req=GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {

            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                Toast.makeText(context,"graph request completed",Toast.LENGTH_SHORT).show();
                try {
                        System.out.println("Objeto entrante: " + response.getJSONObject().toString());
                        if (!response.getJSONObject().isNull("email"))
                        ControladorFacebook.correo = response.getJSONObject().getString("email");
                        if (!response.getJSONObject().isNull("first_name"))
                        ControladorFacebook.nombre = response.getJSONObject().getString("first_name");
                        if (!response.getJSONObject().isNull("last_name"))
                        ControladorFacebook.apellido = response.getJSONObject().getString("last_name");
                        if (!response.getJSONObject().isNull("birthday"))
                        ControladorFacebook.cumpleanos = response.getJSONObject().getString("birthday");
                        if (!response.getJSONObject().isNull("gender"))
                        ControladorFacebook.genero = response.getJSONObject().getString("gender");
                        ControladorFacebook.foto = response.getJSONObject().getJSONObject("picture").getJSONObject("data").getString("url");
                        ControladorFacebook.id = response.getJSONObject().getString("id");
                        if (!response.getJSONObject().isNull("friends"))
                        ControladorFacebook.listaAmigos = response.getJSONObject().getString("friends");
                        //Obteniendo foto de Perfil
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    URL newURL = new URL(ControladorFacebook.foto);
                                    ControladorFacebook.grafico = BitmapFactory.decodeStream(newURL.openConnection().getInputStream());
                                    ControladorFacebook.cargarPerfil(context);

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }).start();
                        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("cookieFacebook", response.getJSONObject().toString());
                        editor.commit();
                        System.out.println("Pase por aqui");


                }catch (Exception e)
                {
                    Toast.makeText(context,"graph request error : "+e.getMessage(),Toast.LENGTH_SHORT).show();
                }

            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "email,first_name,last_name,birthday,gender,picture,friends{name,picture{url}}");
        req.setParameters(parameters);
        req.executeAsync();
    }

    public static void publicarMuro(Fragment fragment, String mensaje, String direccion){
             ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse("https://"+direccion))
                .setQuote(mensaje)
                .build();
             ShareDialog.show(fragment, content);
    }

    public static void cargarPerfil (Context context){
        Intent irfacebook = new Intent(context, PerfilGeneral.class);
        irfacebook.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ControladorGeneral.redactiva = 1;
        context.startActivity(irfacebook);

    }

   public static ArrayList<Amigo> cargarAmigos(){
       ArrayList<Amigo>listado  = new ArrayList<Amigo>();
      try {
           JSONObject lista   = new JSONObject(ControladorFacebook.listaAmigos);
           JSONArray getArray = lista.getJSONArray("data");
           for(int i = 0; i < getArray.length(); i++)
           {
               JSONObject obj = getArray.optJSONObject(i);
               listado.add(new Amigo(obj.getString("id"),
                       null
                       ,obj.getString("name")));
           }

       } catch (JSONException e) {
           e.printStackTrace();
       }


     return listado;
   }


}

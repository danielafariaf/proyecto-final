package com.example.junior.playsocial.Facebook.Model;

import android.graphics.Bitmap;

/**
 * Created by William on 13/7/2017.
 */

public class Amigo {

    private String id;
    private String nombre;
    private Bitmap url;

    public Amigo(String id, String nombre, Bitmap url) {
        this.id = id;
        this.nombre = nombre;
        this.url = url;
    }

    public Amigo(String nombre, String id){
        this.nombre = nombre;
        this.id = id;
    }

    public Amigo(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Bitmap getUrl() {
        return url;
    }

    public void setUrl(Bitmap url) {
        this.url = url;
    }
}

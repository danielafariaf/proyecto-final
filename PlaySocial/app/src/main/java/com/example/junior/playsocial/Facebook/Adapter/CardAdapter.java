package com.example.junior.playsocial.Facebook.Adapter;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.junior.playsocial.Facebook.Model.Amigo;
import com.example.junior.playsocial.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by William on 13/7/2017.
 */

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder>{

    private List<Amigo> lista;

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_card_view, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;

    }

    public CardAdapter(String[] nombres, Bitmap[] fotos){
        super();

        lista = new ArrayList<Amigo>();
        for (int i = 0; i<nombres.length; i++){
            Amigo amigo = new Amigo();
            amigo.setNombre(nombres[i]);
            amigo.setUrl(fotos[i]);
            lista.add(amigo);
        }
    }

    public CardAdapter(){
        super();
        lista = new ArrayList<Amigo>();
    }

    @Override
    public void onBindViewHolder(CardAdapter.ViewHolder holder, int position) {
        Amigo list = lista.get(position);
        holder.textViewTitle.setText(list.getNombre());
        holder.imageView.setImageBitmap(list.getUrl());
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final String TAG = ViewHolder.class.getSimpleName();
        public TextView textViewTitle;
        public ImageView imageView;


        public ViewHolder(View itemView) {
            super(itemView);

            textViewTitle = (TextView) itemView.findViewById(R.id.nombreAmigo);
            imageView = (ImageView) itemView.findViewById(R.id.fotoAmigo);
            itemView.setLongClickable(true);
        }

    }

}

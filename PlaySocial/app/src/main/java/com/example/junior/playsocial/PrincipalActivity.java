package com.example.junior.playsocial;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class PrincipalActivity extends AppCompatActivity {
    ImageView logo;
    ImageView comenzar;
    Animation rotacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        getSupportActionBar().hide();
        logo = (ImageView)findViewById(R.id.logo);
        rotacion= AnimationUtils.loadAnimation(PrincipalActivity.this, R.anim.rotate);
        logo.setAnimation(rotacion);
        //Acceder a inicio de sesion:
        comenzar = (ImageView)findViewById(R.id.comenzar);
        comenzar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent iniciarsesion = new Intent (PrincipalActivity.this,InicioActivity.class);
                startActivity(iniciarsesion);
                finish();
            }
        });


    }
}

package com.example.junior.playsocial.GooglePlus.Fragmentos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.junior.playsocial.ControladorGeneral;
import com.example.junior.playsocial.InicioActivity;
import com.example.junior.playsocial.R;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;


public class PrincipalFragmentoG extends Fragment implements GoogleApiClient.OnConnectionFailedListener {
    private ImageView fotoGoogle;
    private TextView nombreG;
    private TextView emailG;
    private TextView idG;
    private Button BtnCerrarSG;
    private Button BtnRevocarA;


    private GoogleApiClient googleApiClient;
   // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    Activity activity;

    public PrincipalFragmentoG() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
                  GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();

            googleApiClient = new GoogleApiClient.Builder(getActivity())
                    .enableAutoManage((FragmentActivity) getActivity(), this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_principal_fragmento2, container, false);

        fotoGoogle = (ImageView)view.findViewById(R.id.fotoGoogle);
        nombreG = (TextView)view.findViewById(R.id.nombreG);
        emailG = (TextView)view.findViewById(R.id.emailG);
        idG = (TextView)view.findViewById(R.id.idG);
        BtnCerrarSG =(Button)view.findViewById(R.id.BtnCerrarSG);
        BtnRevocarA =(Button)view.findViewById(R.id.BtnRevocarA);

        BtnCerrarSG.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        if (status.isSuccess()) {
                            goLogInScreen();
                        } else {
                            Toast.makeText(getActivity(), R.string.not_close_session, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });



        // Inflate the layout for this fragment
        return view;
    }
    //////////login silencioso para cuando ya esta conectado no tenga q volver a entrar
    @Override
    public void onStart() {
        super.onStart();
        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(googleApiClient);
        if (opr.isDone()) { ///es verdad si ya inicio sesion antes
            GoogleSignInResult result = opr.get();
            handleSignInResult(result); //metodo que maneja el resultado
        }else{
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                    handleSignInResult(googleSignInResult);
                }
            });

        }
    }

    private void handleSignInResult(GoogleSignInResult result) { //peticion silenciosa q devuelve un objeto
        if (result.isSuccess()){

            GoogleSignInAccount account = result.getSignInAccount();

            nombreG.setText("Nombre: "+account.getDisplayName());
            emailG.setText("Correo: "+account.getEmail());
            idG.setText("ID G+: "+account.getId());


            Glide.with(this).load(account.getPhotoUrl()).into(fotoGoogle);

        }else {
            goLogInScreen(); // si no inicio sesion lo manda a la pantalla de login para q lo haga

        }
    }

    private void goLogInScreen() {
        Intent irgoogle = new Intent(getActivity(), InicioActivity.class);
        irgoogle.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        getActivity().startActivity(irgoogle);
    }
  /*  private void logOut(View view){
    BtnCerrarSG.setOnClickListener(new View.OnClickListener(){
        @Override
                public void onClick(View v){
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                if (status.isSuccess()) {
                    goLogInScreen();
                } else {
                    Toast.makeText(getActivity(), R.string.not_close_session, Toast.LENGTH_SHORT).show();
                }
            }
            });
        }
    });}*/

    public void revocaraccsesog(View view) {
        Auth.GoogleSignInApi.revokeAccess(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                if (status.isSuccess()){
                    goLogInScreen();
                }else {
                    Toast.makeText(getActivity(), R.string.not_revoke,Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}

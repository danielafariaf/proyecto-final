package com.example.junior.playsocial;

import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.junior.playsocial.Facebook.Controladores.ControladorFacebook;
//<<<<<<< HEAD
//import com.example.junior.playsocial.Instagram.Actividades.PrincipalInstagram;
//=======
//>>>>>>> e6bb21f8da3983f47cd7cfc2ba89c847e838ff0d
import com.example.junior.playsocial.Instagram.Controladores.ApplicationData;
import com.example.junior.playsocial.Instagram.Controladores.ControladorInstagram;
import com.example.junior.playsocial.Instagram.Controladores.InstagramApp;
import com.example.junior.playsocial.Instagram.Fragmentos.FragmentIterationListener;
import com.example.junior.playsocial.Instagram.Fragmentos.PrincipalFragmentoI;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;


public class InicioActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener, FragmentIterationListener {
    private ImageView instagrambtn;
    private ImageView googlebtn;
    private CallbackManager callbackManager;
    private ImageView facebookbtn;
    private boolean isMainLobbyStarted = false;
    private GoogleApiClient googleApiClient;
    public static final int SING_IN_CODE = 333;
    private ImageView fotoGoogle;
    public TextView nombreG;
    private String update="false";
    private InstagramApp mApp;
    private Button btnConnect;
    private ControladorInstagram controladorInstagram = new ControladorInstagram();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        //Inicializando variables:
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        ControladorFacebook.tokenAutorizacion=null;
        ControladorFacebook.idUsuario=null;
        ControladorFacebook.iniciado = true ;

        //G+
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();

        fotoGoogle = (ImageView) findViewById(R.id.fotoGoogle);
        nombreG = (TextView) findViewById(R.id.nombreG);


        //Colocando el icono en la parte superior izquierda:
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.drawable.logopequeno);
        actionBar.setTitle("");

        //Declarando botones:
        facebookbtn = (ImageView)findViewById(R.id.facebookbtn);
        instagrambtn = (ImageView)findViewById(R.id.instagrambtn);
        googlebtn = (ImageView)findViewById(R.id.googlebtn);

        callbackManager = CallbackManager.Factory.create();


        //Aplicando acciones a botones:
        final LoginManager loginManager = LoginManager.getInstance();
        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                    ControladorFacebook.idUsuario = loginResult.getAccessToken().getUserId();
                    ControladorFacebook.tokenAutorizacion = loginResult.getAccessToken().getToken();
                    ControladorFacebook.appid = loginResult.getAccessToken().getApplicationId();
                    ControladorFacebook.almacenandoFacebook(getApplicationContext(), loginResult.getAccessToken());

            }

            @Override
            public void onCancel() {
                System.out.println("Cancelo");

            }

            @Override
            public void onError(FacebookException e) {
                System.out.println("Dio error: "+ e.getMessage());
                Toast.makeText(getApplicationContext(),"Error de Conexion",Toast.LENGTH_LONG).show();
            }
        });
        facebookbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ControladorGeneral.redactiva = 1;
                loginManager.logInWithReadPermissions(InicioActivity.this,
                        Arrays.asList("user_birthday","email","user_photos","public_profile","user_friends"));


            }
        });

        instagrambtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ControladorGeneral.redactiva = 2;
                cargarPerfilInstagram();

            }
        });
        googlebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent irgoogle = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                ControladorGeneral.redactiva = 3;
                startActivityForResult(irgoogle,SING_IN_CODE);


            }
        });

        //Estableciendo Cookies:
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        String datos = pref.getString("cookieFacebook","vacio");
        if (!datos.equals("vacio")) {
        cargarPerfilFacebook(datos);
        }

    }


    //Metodos propios para la API de Facebook
    //-------------------------------------------------------------------------------------------------------------
    //Metodos para la red social Facebook
    public void cargarPerfilFacebook(String datos){

            try {

                JSONObject informacion = new JSONObject(datos);
                if (!informacion.isNull("email"))
                ControladorFacebook.correo =  informacion.getString("email");
                if (!informacion.isNull("first_name"))
                ControladorFacebook.nombre = informacion.getString("first_name");
                if (!informacion.isNull("last_name"))
                ControladorFacebook.apellido = informacion.getString("last_name");
                if (!informacion.isNull("birthday"))
                ControladorFacebook.cumpleanos = informacion.getString("birthday");
                if (!informacion.isNull("gender"))
                ControladorFacebook.genero = informacion.getString("gender");
                ControladorFacebook.foto = informacion.getJSONObject("picture").getJSONObject("data").getString("url");
                ControladorFacebook.id = informacion.getString("id");
                if (!informacion.isNull("friends"))
                ControladorFacebook.listaAmigos = informacion.getString("friends");
                //Obteniendo foto de Perfil
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            URL newURL = new URL(ControladorFacebook.foto);
                            ControladorFacebook.grafico = BitmapFactory.decodeStream(newURL.openConnection().getInputStream());
                            Intent irfacebook = new Intent(InicioActivity.this, PerfilGeneral.class);
                            ControladorGeneral.redactiva = 1;
                            startActivity(irfacebook);
                        }
                        catch (IOException e)
                        {e.printStackTrace();}
                    }
                }).start();

            } catch (JSONException e) {
                e.printStackTrace();
            }
    }


    //------------------------------------------------------------------------------------------------------------
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SING_IN_CODE){
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }




    @Override
    public void onResume(){
        super.onResume();

    }

    //--------------METODO DE LA RED SOCIAL G+-----------------------------------------------------------------------------
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
    //verifica si conexion exitosa
    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()){
            GoogleSignInAccount account = result.getSignInAccount();

            goMainScreen();
        }else {
            Toast.makeText(this, R.string.not_log_in, Toast.LENGTH_SHORT).show();

        }
    }

    private void goMainScreen() {
        Intent irgoogle = new Intent(this, PerfilGeneral.class);
        irgoogle.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        ControladorGeneral.redactiva = 3;
        startActivity(irgoogle);
    }






    //-------------------------------------------------------------------------------------------------------------
    //Metodos para la red social Instagram
    public void cargarPerfilInstagram() {


        setContentView(R.layout.activity_login_instagram);

        mApp = new InstagramApp(this, ApplicationData.CLIENT_ID,
                ApplicationData.CLIENT_SECRET, ApplicationData.CALLBACK_URL);
        mApp.setListener(new InstagramApp.OAuthAuthenticationListener() {

            @Override
            public void onSuccess() {

                btnConnect.setVisibility(View.INVISIBLE);
                mApp.getUser();

                mApp.fetchUserName(handler);

            }

            @Override
            public void onFail(String error) {
                Toast.makeText(InicioActivity.this, error, Toast.LENGTH_SHORT)
                        .show();
            }
        });
        setWidgetReference();
        bindEventHandlers();

        if (mApp.hasAccessToken()) {
            btnConnect.setText("");
            mApp.fetchUserName(handler);

        }
    }
    private void bindEventHandlers() {
        btnConnect.setOnClickListener(this);
    }

    private void setWidgetReference() {
        btnConnect = (Button) findViewById(R.id.btnConnect);
    }

    // OAuthAuthenticationListener listener ;

    @Override
    public void onClick(View v) {
        if (v == btnConnect) {
            connectOrDisconnectUser();
        }

    }

    private void connectOrDisconnectUser() {
        if (mApp.hasAccessToken()) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(
                    InicioActivity.this);
            builder.setMessage("Disconnect from Instagram?")
                    .setCancelable(false)
                    .setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    mApp.resetAccessToken();
                                    // btnConnect.setVisibility(View.VISIBLE);
                                    //	llAfterLoginView.setVisibility(View.GONE);
                                    btnConnect.setText("Connect");
                                    // tvSummary.setText("Not connected");
                                }
                            })
                    .setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    dialog.cancel();
                                }
                            });
            final AlertDialog alert = builder.create();
            alert.show();
        } else {
            mApp.authorize();
        }
    }



    public void cargarFragment(){

        Intent irinstagram = new Intent(InicioActivity.this,PerfilGeneral.class);
        Log.i("INTENT CARGARF", this.controladorInstagram.getUserName());
        irinstagram.putExtra("controladorInstagram", this.controladorInstagram);
        //Log.i("INTENT CARGARF",(ControladorInstagram)irinstagram.getExtras().get("controladorInstagram").toString());
        startActivity(irinstagram);

    }

    private Handler handler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == InstagramApp.WHAT_FINALIZE) {
                controladorInstagram = mApp.getUser();
                Log.i("USER FULL", controladorInstagram.getFullName());
                cargarFragment();
            } else if (msg.what == InstagramApp.WHAT_FINALIZE) {
                Toast.makeText(InicioActivity.this, "Check your network.",
                        Toast.LENGTH_SHORT).show();
            }
            return false;
        }
    });

}

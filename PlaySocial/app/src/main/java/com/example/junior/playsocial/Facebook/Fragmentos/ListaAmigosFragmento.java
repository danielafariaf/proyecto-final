package com.example.junior.playsocial.Facebook.Fragmentos;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.junior.playsocial.Facebook.Adapter.CardAdapter;
import com.example.junior.playsocial.Facebook.Controladores.ControladorFacebook;
import com.example.junior.playsocial.Facebook.Controladores.GetBitmap;
import com.example.junior.playsocial.Facebook.Model.ListaAmigo;
import com.example.junior.playsocial.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class ListaAmigosFragmento extends Fragment {

    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;


    public ListaAmigosFragmento() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lista_amigos_fragmento, container, false);

        recycler = (RecyclerView) view.findViewById(R.id.recycler);
        adapter = new CardAdapter();
        layoutManager = new GridLayoutManager(getActivity(),2);
        recycler.setLayoutManager(layoutManager);
        recycler.setAdapter(adapter);
        recycler.setHasFixedSize(true);
        Log.i("TAG", ControladorFacebook.listaAmigos);
        cargarAmigos();
        return view;
    }

    public void onResume(){
        super.onResume();
        cargarAmigos();
    }

    private void cargarAmigos() {
        JSONObject datos;
        JSONArray array;

        try{
            datos = new JSONObject(ControladorFacebook.listaAmigos);
            array = datos.getJSONArray("data");
            new ListaAmigo(array.length());
            for(int i=0; i<array.length(); i++){
                JSONObject o = array.getJSONObject(i);
                ListaAmigo.nombres[i] = o.getString("name");
                ListaAmigo.urls[i] = o.getJSONObject("picture").getJSONObject("data").getString("url");
            }
        } catch (JSONException e){
            Log.e("ERROR",e.toString());
        }

        GetBitmap gb = new GetBitmap(getActivity(), this, ListaAmigo.urls);
        gb.execute();
    }

    public void showData(){
        adapter = new CardAdapter(ListaAmigo.nombres, ListaAmigo.imagenes);
        recycler.setAdapter(adapter);
    }


}

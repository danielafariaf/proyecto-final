package com.example.junior.playsocial.Instagram.Controladores;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by karo on 12/07/17.
 */
@SuppressWarnings("serial")
public class ControladorInstagram implements Serializable {
    @SerializedName("user")
    private String fullName;
    @SerializedName("fullname")
    private String userName;
    @SerializedName("id")
    private int id;
    @SerializedName("profileimageurl")
    private String profileImageURL;
    @SerializedName("followers")
    private int followers;
    @SerializedName("following")
    private int following;
    @SerializedName("bio")
    private String bio;
    @SerializedName("website")
    private String website;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProfileImageURL() {
        return profileImageURL;
    }

    public void setProfileImageURL(String profileImageURL) {
        this.profileImageURL = profileImageURL;
    }

    public int getFollowers() {
        return followers;
    }

    public void setFollowers(int followers) {
        this.followers = followers;
    }

    public int getFollowing() {
        return following;
    }

    public void setFollowing(int following) {
        this.following = following;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}

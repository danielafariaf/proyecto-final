package com.example.junior.playsocial.Instagram.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.support.multidex.MultiDex;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.example.junior.playsocial.Instagram.Fragmentos.FragmentIterationListener;
import com.example.junior.playsocial.Instagram.Controladores.InstagramApp.OAuthAuthenticationListener;
import com.example.junior.playsocial.Instagram.Controladores.ApplicationData;
import com.example.junior.playsocial.Instagram.Controladores.InstagramApp;
import com.example.junior.playsocial.Instagram.Controladores.ControladorInstagram;
import com.example.junior.playsocial.Instagram.Fragmentos.PrincipalFragmentoI;
import com.example.junior.playsocial.R;

public class InstagramPrincipalActivity extends Activity implements OnClickListener, FragmentIterationListener {

	private InstagramApp mApp;
	private Button btnConnect;
	private ControladorInstagram controladorInstagram = new ControladorInstagram();
	private Handler handler = new Handler(new Callback() {

		@Override
		public boolean handleMessage(Message msg) {
			if (msg.what == InstagramApp.WHAT_FINALIZE) {
				controladorInstagram = mApp.getUser();
				Log.i("USER FULL", controladorInstagram.getFullName());
				cargarFragment();
			} else if (msg.what == InstagramApp.WHAT_FINALIZE) {
				Toast.makeText(InstagramPrincipalActivity.this, "Check your network.",
						Toast.LENGTH_SHORT).show();
			}
			return false;
		}
	});


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		MultiDex.install(this);

		setContentView(R.layout.activity_login_instagram);

		mApp = new InstagramApp(this, ApplicationData.CLIENT_ID,
				ApplicationData.CLIENT_SECRET, ApplicationData.CALLBACK_URL);
		mApp.setListener(new OAuthAuthenticationListener() {

			@Override
			public void onSuccess() {

				btnConnect.setVisibility(View.INVISIBLE);
				mApp.getUser();

				mApp.fetchUserName(handler);

			}

			@Override
			public void onFail(String error) {
				Toast.makeText(InstagramPrincipalActivity.this, error, Toast.LENGTH_SHORT)
						.show();
			}
		});
		setWidgetReference();
		bindEventHandlers();

		if (mApp.hasAccessToken()) {
			btnConnect.setText("Disconnect");
			mApp.fetchUserName(handler);

		}

	}

	private void bindEventHandlers() {
		btnConnect.setOnClickListener(this);
	}

	private void setWidgetReference() {
		btnConnect = (Button) findViewById(R.id.btnConnect);
	}

	// OAuthAuthenticationListener listener ;

	@Override
	public void onClick(View v) {
		if (v == btnConnect) {
			connectOrDisconnectUser();
		}

	}

	private void connectOrDisconnectUser() {
		if (mApp.hasAccessToken()) {
			final AlertDialog.Builder builder = new AlertDialog.Builder(
					InstagramPrincipalActivity.this);
			builder.setMessage("Disconnect from Instagram?")
					.setCancelable(false)
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									mApp.resetAccessToken();
									btnConnect.setText("Connect");
								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
								}
							});
			final AlertDialog alert = builder.create();
			alert.show();
		} else {
			mApp.authorize();
		}
	}



	public void cargarFragment(){
		Log.i("CARGARFRAGMENT",this.controladorInstagram.getUserName());
		PrincipalFragmentoI fragment = PrincipalFragmentoI.newInstance(this.controladorInstagram, getBaseContext());

		FragmentTransaction ft = getFragmentManager().beginTransaction();
		ft.replace(R.id.activity_login_instagram, fragment);
		ft.commit();
	}
}
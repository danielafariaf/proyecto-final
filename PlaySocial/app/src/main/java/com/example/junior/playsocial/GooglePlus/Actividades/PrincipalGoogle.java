package com.example.junior.playsocial.GooglePlus.Actividades;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.junior.playsocial.InicioActivity;
import com.example.junior.playsocial.R;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

public class PrincipalGoogle extends AppCompatActivity {//implements GoogleApiClient.OnConnectionFailedListener {

    private ImageView fotoGoogle;
    private TextView nombreG;
    private TextView emailG;
    private TextView idG;

    private GoogleApiClient googleApiClient;

  /* @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal_google);

        fotoGoogle = (ImageView) findViewById(R.id.fotoGoogle);
        nombreG = (TextView) findViewById(R.id.nombreG);
        emailG = (TextView) findViewById(R.id.emailG);
        idG = (TextView) findViewById(R.id.idG);


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();
    }
    //////////login silencioso para cuando ya esta conectado no tenga q volver a entrar
    @Override
    protected void onStart() {
        super.onStart();
        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(googleApiClient);
        if (opr.isDone()) { ///es verdad si ya inicio sesion antes
            GoogleSignInResult result = opr.get();
            handleSignInResul(result); //metodo que maneja el resultado
        }else{
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                    handleSignInResul(googleSignInResult);
                }
            });

        }
    }

    private void handleSignInResult(GoogleSignInResult result) { //peticion silenciosa q devuelve un objeto
        if (result.isSuccess()){

            GoogleSignInAccount account = result.getSignInAccount();

            nombreG.setText(account.getDisplayName());
            emailG.setText(account.getEmail());
            idG.setText(account.getId());

            Glide.with(this).load(account.getPhotoUrl()).into(fotoGoogle);

        }else {
            goLogInScreen(); // si no inicio sesion lo manda a la pantalla de login para q lo haga

        }
    }

    private void goLogInScreen() {
        Intent irgoogle = new Intent(this, InicioActivity.class);
        irgoogle.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(irgoogle);
    }


    public void logOut(View view) {
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                if (status.isSuccess()){
                    goLogInScreen();
                }else {
                    Toast.makeText(getApplicationContext(), R.string.not_close_session,Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    public void revocaraccsesog(View view) {
        Auth.GoogleSignInApi.revokeAccess(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                if (status.isSuccess()){
                    goLogInScreen();
                }else {
                    Toast.makeText(getApplicationContext(), R.string.not_revoke,Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }*/
}

package com.example.junior.playsocial.Facebook.Fragmentos;

import android.content.Context;
import android.content.pm.PackageInstaller;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.junior.playsocial.Facebook.Controladores.ControlFotos;
import com.example.junior.playsocial.Facebook.Controladores.ControladorFacebook;
import com.example.junior.playsocial.R;


public class PrincipalFragmentoF extends Fragment  {



    public PrincipalFragmentoF() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_principal_fragmento, container, false);
        TextView nombre = (TextView)view.findViewById(R.id.nombreF);
        nombre.setText("Nombre: "+ControladorFacebook.nombre);
        TextView apellido = (TextView)view.findViewById(R.id.apellidoF);
        apellido.setText("Apellido: " + ControladorFacebook.apellido);
        TextView correo = (TextView)view.findViewById(R.id.correoF);
        correo.setText("Correo: " + ControladorFacebook.correo);
        TextView nacimiento = (TextView)view.findViewById(R.id.nacimientoF);
        nacimiento.setText("Nacimiento: " +ControladorFacebook.cumpleanos);
        TextView genero = (TextView)view.findViewById(R.id.generoF);
        genero.setText("Genero: " + ControladorFacebook.genero);
        ImageView fotoPerfil = (ImageView)view.findViewById(R.id.fotoPerfil);
        fotoPerfil.setImageBitmap(ControladorFacebook.grafico);

        return view;
    }






}

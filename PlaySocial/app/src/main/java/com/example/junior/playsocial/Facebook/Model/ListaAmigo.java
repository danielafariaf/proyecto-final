package com.example.junior.playsocial.Facebook.Model;

import android.graphics.Bitmap;
import android.util.Log;

import com.example.junior.playsocial.Facebook.Controladores.ControladorFacebook;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by William on 13/7/2017.
 */

public class ListaAmigo {

    public static String[] nombres;
    public static String[] urls;
    public static Bitmap[] imagenes;
    private List<Amigo> lista;

    public ListaAmigo(int i){
        nombres = new String[i];
        urls = new String[i];
        imagenes = new Bitmap[i];
        lista = new ArrayList<>();
    }

    @Override
    public String toString() {
        String resp="";

        for(String r : nombres) {
            resp+=" "+r;
        }

        for(String r : urls)
            resp+="  "+r;

        return resp;
    }



}
